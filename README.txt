A simple simulation that shows the signal obtained in certain streaking setups. 

The signal is the integral from the a given point in the pump pulse to the end multiplied by the probe pulse. The red line marks the value that results when the probe pulse is offset as shown.
