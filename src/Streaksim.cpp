#include "Streaksim.h"

Streaksim::Streaksim(QWidget* parent) : QWidget(parent)
{
	glayout = new QGridLayout(this);
	glayout->setContentsMargins(0, 0, 0, 0); //Left Top Right Bottom
	glayout->setSpacing(0);
	this->setLayout(glayout);

	sidebar = new Sidebar(this);
	sidebar->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
	glayout->addWidget(sidebar,0, 0, -1, 1);

	pulsePlot = new QCustomPlot(this);
	pulsePlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	glayout->addWidget(pulsePlot, 0, 1);

	signalPlot = new QCustomPlot(this);
	signalPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	glayout->addWidget(signalPlot, 1, 1);

	connectAll();

	initPlottingValues();
	initPlots();

	initDefaultValues(); //setSigma etc. only after plots are initiated because of refresh/recalc

	recalculate();
	refresh();
}

void Streaksim::connectAll()
{
	QObject::connect(sidebar->getSigmaCon(), SIGNAL(valueChanged(double)), this, SLOT(setSigma(double)));
	QObject::connect(sidebar->getOmegaCon(), SIGNAL(valueChanged(double)), this, SLOT(setOmega(double)));
	QObject::connect(sidebar->getCEPCon(), SIGNAL(valueChanged(double)), this, SLOT(setCEP(double)));
	QObject::connect(sidebar->getProbeSigmaCon(), SIGNAL(valueChanged(double)), this, SLOT(setProbeSigma(double)));
	QObject::connect(sidebar->getProbeOffsetCon(), SIGNAL(valueChanged(double)), this, SLOT(setProbeOffset(double)));
	QObject::connect(sidebar->getNumPointsCon(), SIGNAL(valueChanged(double)), this, SLOT(setNumPoints(double)));
}


QSize Streaksim::sizeHint() const
{
	return QSize(1100,700);
}

void Streaksim::initPlottingValues()
{
	xmin = -10.0;
	xmax = 10.0;
	ymin = -1.0;
	ymax = 1.0;
	numPoints = 20;//gets set again in initDefaultValues
}

void Streaksim::initDefaultValues()
{
	sidebar->getSigmaCon()->setRange(1.0e-3, 5.0);
	sidebar->getSigmaCon()->setValue(1.0);

	sidebar->getOmegaCon()->setRange(0.0, 20.0);
	sidebar->getOmegaCon()->setValue(5.0);

	sidebar->getCEPCon()->setRange(0.0, 2*3.14159265);
	sidebar->getCEPCon()->setValue(0.0);

	sidebar->getProbeSigmaCon()->setRange(1.0e-3, 3.0);
	sidebar->getProbeSigmaCon()->setValue(0.3);

	sidebar->getProbeOffsetCon()->setRange(xmin, xmax);
	sidebar->getProbeOffsetCon()->setValue(0.0);

	sidebar->getNumPointsCon()->setRange(5, 1000);
	sidebar->getNumPointsCon()->setValue(301);
}
 
void Streaksim::initPlots()
{
	pulsePlot->addGraph();
	pulsePlot->addGraph();
	pulsePlot->graph(1)->setPen(Qt::DashLine);
	pulsePlot->xAxis->setRange(xmin,xmax);
	pulsePlot->yAxis->setRange(ymin,ymax);

	signalPlot->addGraph();
	signalPlot->xAxis->setRange(xmin, xmax);
	signalPlot->yAxis->setRange(ymin, ymax);
	marker = new QCPItemLine(signalPlot);
	marker->setPen(QPen(Qt::red));
}

void Streaksim::recalculate()
{
	generateTime();
	generatePulse();
	generateProbe();
	generateSignal();
}

void Streaksim::refresh()
{
	pulsePlot->graph(0)->setData(QVector<double>::fromStdVector(time),QVector<double>::fromStdVector(pulse));
	pulsePlot->graph(1)->setData(QVector<double>::fromStdVector(time),QVector<double>::fromStdVector(probe));
	pulsePlot->replot();

	signalPlot->graph(0)->setData(QVector<double>::fromStdVector(time),QVector<double>::fromStdVector(signal));
	marker->start->setCoords(probeOffset,signalPlot->yAxis->range().lower);
	marker->end->setCoords(probeOffset,signalPlot->yAxis->range().upper);
	signalPlot->replot();
}

void Streaksim::generateTime()
{
	time.clear();
	for (int i=0; i<numPoints; ++i)
	{
		double t = xmin + (xmax-xmin)/(numPoints-1) * i;
		time.push_back(t);
	}
}

void Streaksim::generatePulse()
{
	pulse.clear();
	for(int i=0; i<numPoints; ++i)
	{
		double E = exp(-pow(time[i]/sigma, 2)/2) * cos(omega*time[i]-CEP);
		pulse.push_back(E);
	}
}

void Streaksim::generateProbe()
{
	probe.clear();
	for(int i=0; i<numPoints; ++i)
	{
		double E = exp(-pow((time[i]-probeOffset)/probeSigma, 2)/2);
		probe.push_back(E);
	}
}

void Streaksim::generateSignal()
{
	signal.clear();
	double origOffset = probeOffset;
	double threshold = 0.5;
	for(int i=0; i<numPoints; ++i)
	{
		probeOffset = time[i];
		generateProbe();
		double tempSignal = 0;
		for(int j=0; j<numPoints; j++)
		{
			tempSignal += probe[j]*pulse[j];
		}
		signal.push_back(tempSignal);
	}

	//the correct way to normalize would be to find the settings where the signal is signal is extrremal and uses its maximum as the divisor. Some Settings are ridiculous though.
	//double maxSignal = *max_element(std::begin(signal), std::end(signal));
	for(int i=0; i<signal.size(); i++)
	{
		//signal[i]/=maxSignal;
		signal[i]/=30.0;
	}

	probeOffset = origOffset;
	generateProbe();
}


void Streaksim::setSigma(double in)
{
	sigma = in;
	recalculate();
	refresh();
}

void Streaksim::setOmega(double in)
{
	omega = in;
	recalculate();
	refresh();
}

void Streaksim::setCEP(double in)
{
	CEP = in;
	recalculate();
	refresh();
}

void Streaksim::setProbeSigma(double in)
{
	probeSigma = in;
	recalculate();
	refresh();
}

void Streaksim::setProbeOffset(double in)
{
	probeOffset = in;
	recalculate();
	refresh();
}

void Streaksim::setNumPoints(double in)
{
	numPoints = (int)in;
	recalculate();
	refresh();
}
