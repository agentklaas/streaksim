#pragma once

#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDoubleSpinBox>
#include <QLabel>

//#include "QScienceSpinBox.h"
#include "QController.h"

class Sidebar : public QWidget
{
	private:
	QVBoxLayout* vlayout;

	QController *sigmaCon, *omegaCon, *CEPCon;
	QController *probeSigmaCon, *probeOffsetCon;
	QController *numPointsCon;

	public:
	Sidebar(QWidget* parent);
	QSize sizeHint() const override;

	QController* getSigmaCon();
	QController* getOmegaCon();
	QController* getCEPCon();
	QController* getProbeSigmaCon();
	QController* getProbeOffsetCon();
	QController* getNumPointsCon();
};
