#include "Sidebar.h"

Sidebar::Sidebar(QWidget* parent) : QWidget(parent)
{
	vlayout = new QVBoxLayout(this);
	this->setLayout(vlayout);
	vlayout->setContentsMargins(2,2,2,2);
	vlayout->setSpacing(20);
	
	sigmaCon = new QController(this, "sigma");
	vlayout->addWidget(sigmaCon);

	omegaCon = new QController(this, "omega");
	vlayout->addWidget(omegaCon);

	CEPCon = new QController(this, "CEP");
	vlayout->addWidget(CEPCon);

	probeSigmaCon = new QController(this, "probe sigma");
	vlayout->addWidget(probeSigmaCon);

	probeOffsetCon = new QController(this, "probe offset");
	vlayout->addWidget(probeOffsetCon);

	numPointsCon = new QController(this, "Number of Points");
	vlayout->addWidget(numPointsCon);


	QWidget* spacer = new QWidget(this);
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	vlayout->addWidget(spacer);

	////Colors for testing
	//this->setAutoFillBackground(true);
	//QPalette pal;
	//pal.setColor(QPalette::Background, Qt::green);
	//this->setPalette(pal);
}

QSize Sidebar::sizeHint() const
{
	return QSize(300,300);
}

QController* Sidebar::getSigmaCon()
{
	return sigmaCon;
}

QController* Sidebar::getOmegaCon()
{
	return omegaCon;
}

QController* Sidebar::getCEPCon()
{
	return CEPCon;
}

QController* Sidebar::getProbeSigmaCon()
{
	return probeSigmaCon;
}

QController* Sidebar::getProbeOffsetCon()
{
	return probeOffsetCon;
}

QController* Sidebar::getNumPointsCon()
{
	return numPointsCon;
}


