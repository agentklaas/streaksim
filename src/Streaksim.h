#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QVector>

//#include <iostream>
#include <vector>

#include "qcustomplot.h"

#include "Sidebar.h"

class Streaksim : public QWidget
{
	Q_OBJECT

private:
	std::vector<double> time;
	std::vector<double> pulse; //preferring usage of std::vector over QVector
	std::vector<double> probe;
	std::vector<double> signal;

	double sigma, omega, CEP;
	double probeSigma, probeOffset;
	int numPoints;
	double xmin, xmax, ymin, ymax;
	double Threshold;


	QGridLayout* glayout;
	Sidebar* sidebar;
	QCustomPlot* pulsePlot;
	QCustomPlot* signalPlot;
	QCPItemLine* marker;

	void connectAll();

public:
	Streaksim(QWidget* parent=nullptr);
	QSize sizeHint() const override;

	void initPlottingValues();
	void initDefaultValues();
	void initPlots();
	void recalculate();
	void refresh();

	void generateTime();
	void generatePulse();
	void generateProbe();
	void generateSignal();

public slots:
	void setSigma(double in);
	void setOmega(double in);
	void setCEP(double in);
	void setProbeSigma(double in);
	void setProbeOffset(double in);
	void setNumPoints(double in);
};
